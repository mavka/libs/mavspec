#[test]
#[cfg(feature = "dlct-all")]
fn test_all_is_default_dialect() {
    use mavspec::rust::spec::Dialect;
    let dialect_spec = mavspec::rust::DefaultDialect::spec();
    assert_eq!(dialect_spec.name(), "all");
}
