#[test]
#[cfg(feature = "dlct-all")]
fn test_inheritance() {
    use mavspec::rust::dialects::all;
    use mavspec::rust::dialects::ardupilotmega;
    use mavspec::rust::dialects::common;
    use mavspec::rust::dialects::minimal;

    all::messages::CommandInt {
        frame: all::enums::MavFrame::Mission,
        command: all::enums::MavCmd::DoSetRelay,
        ..Default::default()
    };

    all::messages::CommandInt {
        frame: ardupilotmega::enums::MavFrame::Mission,
        command: all::enums::MavCmd::DoSetRelay,
        ..Default::default()
    };

    all::messages::CommandInt {
        frame: common::enums::MavFrame::Mission,
        command: all::enums::MavCmd::DoSetRelay,
        ..Default::default()
    };

    ardupilotmega::messages::CommandInt {
        frame: common::enums::MavFrame::Mission,
        command: ardupilotmega::enums::MavCmd::DoSetRelay,
        ..Default::default()
    };

    all::messages::Heartbeat {
        type_: minimal::enums::MavType::Adsb,
        autopilot: minimal::enums::MavAutopilot::Aerob,
        base_mode: minimal::enums::MavModeFlag::all(),
        system_status: minimal::enums::MavState::Active,
        ..Default::default()
    };

    all::All::Heartbeat(minimal::messages::Heartbeat::default());
}
