#[test]
#[cfg(feature = "extra-dialects")]
fn test_extra_dialects() {
    use mavspec::rust::dialects::example_mav_spec_dialect as dialect;
    let _ = dialect::ExampleMavSpecDialect::ExampleMessage(
        dialect::messages::ExampleMessage::default(),
    );
}

#[test]
#[cfg(feature = "test-dialects")]
fn test_test_dialects() {
    use mavspec::rust::dialects::mav_inspect_test as dialect;
    let _ = dialect::MavInspectTest::MavInspectV1(dialect::messages::MavInspectV1::default());
}
