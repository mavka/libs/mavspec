#[test]
#[cfg(feature = "std")]
#[cfg(feature = "serde")]
#[cfg(feature = "unstable")]
#[cfg(feature = "msrv-utils-mission")]
fn test_waypoints_serialization() {
    use mavspec::rust::microservices::mission::{MissionPlan, Waypoint};

    let mut mission = MissionPlan::new();
    mission.push(Waypoint::default());
    mission.push(Waypoint::default());

    let serialized = serde_json::to_string(&mission).unwrap();
    let deserialized: MissionPlan = serde_json::from_str(&serialized).unwrap();

    assert!(!deserialized.is_empty());
    assert_eq!(deserialized.len(), 2);
}
