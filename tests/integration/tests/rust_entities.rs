#[test]
#[cfg(feature = "dlct-common")]
fn test_enums_are_comparable() {
    use mavspec::rust::default_dialect::enums::{MavModeFlag, MavState};

    assert_eq!(
        MavModeFlag::HIL_ENABLED | MavModeFlag::AUTO_ENABLED,
        MavModeFlag::HIL_ENABLED | MavModeFlag::AUTO_ENABLED
    );

    assert_eq!(MavState::Critical, MavState::Critical);
}

#[test]
#[cfg(feature = "dlct-common")]
fn test_messages_are_comparable() {
    use mavspec::rust::default_dialect::messages::Heartbeat;

    assert_eq!(Heartbeat::default(), Heartbeat::default());
}

#[test]
#[cfg(feature = "dlct-common")]
fn test_dialect_enums_are_comparable() {
    use mavspec::rust::default_dialect::messages::Heartbeat;
    use mavspec::rust::DefaultDialect;

    assert_eq!(
        DefaultDialect::Heartbeat(Heartbeat::default()),
        DefaultDialect::Heartbeat(Heartbeat::default())
    );
}
