#[test]
#[cfg(feature = "std")]
#[cfg(feature = "dlct-common")]
#[cfg(feature = "definitions")]
fn test_basic_metadata() {
    let dialect_spec = mavspec::definitions::protocol()
        .get_dialect_by_name("common")
        .unwrap();
    let message_spec = dialect_spec.get_message_by_name("HEARTBEAT").unwrap();
    assert_eq!(message_spec.name(), "HEARTBEAT");
}
