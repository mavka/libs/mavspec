#[test]
fn test_patched_definitions() {
    use dialect::enums::{ExampleEnum, ExtraEnum};
    use dialect::messages::ExtraMessage;
    use mavspec::rust::dialects::extra_mav_spec_dialect as dialect;

    let _ = ExtraMessage {
        example_enum_field: ExampleEnum::ExtraTwo,
        example_extra_enum_field: ExtraEnum::Zero,
    };
}
