Example patch for `mavlink-message-definitions`
===============================================

This crate shows how to patch [MAVSpec](https://crates.io/crates/mavspec) and all upstream crates with custom message
definitions.

How this works
--------------

We use native Cargo [path](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html#the-patch-section)
mechanism to replace [`mavlink-message-definitions`](https://crates.io/crates/mavlink-message-definitions). In the
[`Cargo.toml`](Cargo.toml) we simply added:

```toml
[patch.crates-io]
mavlink-message-definitions = { git = "https://gitlab.com/mavka/spec/protocols/mavlink/mavspec-definitions.git", branch = "extras" }
```

That replaced standard `mavlink-message-definitions` crate with a patched version from the
[extras](https://gitlab.com/mavka/spec/protocols/mavlink/mavspec-definitions/-/tree/extras) that contains additional
dialects.

We've also enabled `extra-dialects` feature flag to `mavspec` dependency for loading these extra definitions.

Now, the following becomes possible (these MAVLink entities are not available in standard definitions):

```rust
use dialect::enums::{ExampleEnum, ExtraEnum};
use dialect::messages::ExtraMessage;
use mavspec::rust::dialects::extra_mav_spec_dialect as dialect;

fn main() {
    let message = ExtraMessage {
        example_enum_field: ExampleEnum::ExtraTwo,
        example_extra_enum_field: ExtraEnum::Zero,
    };

    /* Do something important */
}
```

Test
----

Run with default dialects:

```shell
cargo test --package mavspec_examples_patch_message_definitions --all-features
```
