#!/usr/bin/env bash
#
# Go over all significant build variants
#

set -o errexit -o nounset -o pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROJECT_DIR=$(realpath "${SCRIPT_DIR}/../../")

HL='\033[0;35m'
NC='\033[0m'

function print() {
  echo -e "$HL$1$NC"
}

declare -a commands=(
  "cargo build --package mavspec --no-default-features --features rust"
  "cargo build --package mavspec --no-default-features --features rust,std"
  "cargo build --package mavspec --no-default-features --features rust,std,serde,unstable"
  "cargo build --package mavspec --no-default-features --features rust,msrv-all"
  "cargo build --package mavspec --no-default-features --features rust,msrv-all,specta,std,unstable"
  "cargo build --package mavspec --no-default-features --features rust,dlct-all"
  "cargo build --package mavspec --no-default-features --features rust,dlct-all,definitions,std"
  "cargo build --package mavspec --no-default-features --features rust,dlct-all,msrv-all,msrv-utils-all,serde"
  "cargo build --package mavspec --no-default-features --features rust,dlct-all,msrv-all,msrv-utils-all,serde,specta,std,unstable"
  "cargo build --package mavspec --all-features"
)

pushd "${PROJECT_DIR}"
  for command in "${commands[@]}"
  do
    print "$command"
    time $command
  done
popd
