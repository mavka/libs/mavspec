//! <sup>`⍚`</sup> MAVLink [mission protocol](https://mavlink.io/en/services/mission.html) utils.
//!
//! Supports unofficial [mission file format](https://mavlink.io/en/file_formats/#mission_plain_text_file)
//! using [`MissionPlan`].

mod consts;
mod error;
#[cfg(feature = "alloc")]
mod mission_plan;
mod mission_plan_no_alloc;
mod waypoint;

pub use error::MissionError;
pub use waypoint::Waypoint;

#[cfg(feature = "alloc")]
pub use mission_plan::MissionPlan;
#[cfg(not(feature = "alloc"))]
#[doc(inline)]
pub use no_alloc::MissionPlan;

/// `no-alloc` mission protocol tools.
pub mod no_alloc {
    pub use super::mission_plan_no_alloc::MissionPlan;
}
