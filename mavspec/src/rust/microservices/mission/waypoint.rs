use super::error::MissionError;
use crate::rust::default_dialect::microservices::mission;
use crate::rust::spec::types::MavLinkId;
use crate::rust::spec::SpecError;
use mission::enums::{MavCmdWaypoint, MavFrame, MavMissionType};
use mission::messages::MissionItem;

/// <sup>`⍚`</sup> MAVLink mission waypoint.
#[cfg_attr(all(feature = "specta", feature = "unstable"), derive(specta::Type))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Copy, Debug)]
pub struct Waypoint {
    /// Waypoint index.
    pub index: u16,
    /// Is a current MAVLink waypoint.
    pub current: bool,
    /// MAVLink field `frame`.
    ///
    /// The coordinate system of the waypoint.
    pub frame: MavFrame,
    /// MAVLink field `command`.
    ///
    /// The scheduled action for the waypoint.
    pub command: MavCmdWaypoint,
    /// MAVLink command field `param1`.
    pub param1: f32,
    /// MAVLink command field `param2`.
    pub param2: f32,
    /// MAVLink command field `param3`.
    pub param3: f32,
    /// MAVLink command field `param4`.
    pub param4: f32,
    /// MAVLink command field `x`.
    ///
    /// PARAM5 / local: x position in meters * 1e4, global: latitude in degrees * 10^7
    pub x: f64,
    /// MAVLink command field `y`.
    ///
    /// PARAM6 / y position: local: x position in meters * 1e4, global: longitude in degrees *10^7
    pub y: f64,
    /// MAVLink command field `z`.
    ///
    /// PARAM7 / z position: global: altitude in meters (relative or absolute, depending on frame.
    pub z: f32,
    /// MAVLink field `autocontinue`.
    ///
    /// Autocontinue to next waypoint. Set false to pause mission after the item completes.
    pub autocontinue: bool,
}

macro_rules! waypoint_fmt_arguments {
    ($subject:ident) => {
        format_args!(
            "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
            $subject.index,
            $subject.current as u8,
            $subject.frame as u8,
            $subject.command as u16,
            $subject.param1,
            $subject.param2,
            $subject.param3,
            $subject.param4,
            $subject.x,
            $subject.y,
            $subject.z,
            $subject.autocontinue as u8,
        )
    };
}

impl Waypoint {
    /// Converts waypoint into a [`MissionItem`] message.
    pub fn to_mission_item(&self, target_id: MavLinkId) -> MissionItem {
        MissionItem {
            target_system: target_id.system,
            target_component: target_id.component,
            seq: self.index,
            frame: self.frame,
            command: self.command.into(),
            current: self.current.into(),
            autocontinue: self.autocontinue.into(),
            param1: self.param1,
            param2: self.param2,
            param3: self.param3,
            param4: self.param4,
            x: self.x as f32,
            y: self.y as f32,
            z: self.z,
            mission_type: MavMissionType::Mission,
        }
    }

    /// Tries to create an `&str` representations of waypoint using a provided buffer.
    ///
    /// Supports `no-alloc` targets. Use `to_string` otherwise.
    pub fn as_str_buffered<'a>(&self, buf: &'a mut [u8]) -> Result<&'a str, MissionError> {
        format_no_std::show(&mut buf[..], waypoint_fmt_arguments!(self))
            .map_err(|_| MissionError::BufferTooSmall)
    }

    /// Tries to create a byte representations of waypoint using a provided buffer.
    ///
    /// Supports `no-alloc` targets. Use `to_string` and pass its result as bytes otherwise.
    pub fn as_bytes_buffered<'a>(&self, buf: &'a mut [u8]) -> Result<&'a [u8], MissionError> {
        Ok(self.as_str_buffered(buf)?.as_bytes())
    }
}

impl Default for Waypoint {
    fn default() -> Self {
        Self {
            index: 0,
            current: false,
            frame: MavFrame::Mission,
            command: MavCmdWaypoint::NavTakeoff,
            param1: 0.0,
            param2: 0.0,
            param3: 0.0,
            param4: 0.0,
            x: 0.0,
            y: 0.0,
            z: 0.0,
            autocontinue: true,
        }
    }
}

impl core::fmt::Display for Waypoint {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(waypoint_fmt_arguments!(self))
    }
}

impl TryFrom<MissionItem> for Waypoint {
    type Error = SpecError;

    fn try_from(value: MissionItem) -> Result<Self, Self::Error> {
        Ok(Self {
            index: value.seq,
            current: value.current != 0,
            frame: value.frame,
            command: value.command.try_into()?,
            param1: value.param1,
            param2: value.param2,
            param3: value.param3,
            param4: value.param4,
            x: value.x as f64,
            y: value.y as f64,
            z: value.z,
            autocontinue: value.autocontinue != 0,
        })
    }
}

impl TryFrom<&[u8]> for Waypoint {
    type Error = MissionError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        use core::str;
        Self::try_from(str::from_utf8(value).map_err(|_| Self::Error::InvalidUtf8)?)
    }
}

impl TryFrom<&str> for Waypoint {
    type Error = MissionError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut waypoint = Waypoint::default();
        let mut max_ixd = 0;

        for (idx, entry) in value.split("\t").map(|entry| entry.trim()).enumerate() {
            max_ixd = core::cmp::max(max_ixd, idx);
            match idx {
                0 => {
                    waypoint.index = entry
                        .parse::<u16>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                1 => {
                    waypoint.current = entry
                        .parse::<u8>()
                        .map_err(|_| Self::Error::ParseField(idx))?
                        != 0;
                }
                2 => {
                    waypoint.frame = MavFrame::try_from(
                        entry
                            .parse::<u8>()
                            .map_err(|_| Self::Error::ParseField(idx))?,
                    )
                    .map_err(|_| Self::Error::ParseField(idx))?;
                }
                3 => {
                    waypoint.command = MavCmdWaypoint::try_from(
                        entry
                            .parse::<u16>()
                            .map_err(|_| Self::Error::ParseField(idx))?,
                    )
                    .map_err(|_| Self::Error::ParseField(idx))?;
                }
                4 => {
                    waypoint.param1 = entry
                        .parse::<f32>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                5 => {
                    waypoint.param2 = entry
                        .parse::<f32>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                6 => {
                    waypoint.param3 = entry
                        .parse::<f32>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                7 => {
                    waypoint.param4 = entry
                        .parse::<f32>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                8 => {
                    waypoint.x = entry
                        .parse::<f64>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                9 => {
                    waypoint.y = entry
                        .parse::<f64>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                10 => {
                    waypoint.z = entry
                        .parse::<f32>()
                        .map_err(|_| Self::Error::ParseField(idx))?;
                }
                11 => {
                    waypoint.autocontinue = entry
                        .parse::<u8>()
                        .map_err(|_| Self::Error::ParseField(idx))?
                        != 0;
                }
                _ => {}
            }
        }

        if max_ixd < 11 {
            return Err(Self::Error::MissingField(max_ixd));
        }

        Ok(waypoint)
    }
}

#[cfg(test)]
mod waypoint_tests {
    use super::*;

    #[test]
    fn test_parse_waypoint_from_str() {
        let line = "12\t1\t2\t16\t0.149999999999999994\t2.0\t3.0\t4.0\t8.54800000000000004\t47.3759999999999977\t550\t1";
        let waypoint = Waypoint::try_from(line).unwrap();

        assert_eq!(waypoint.index, 12);
        assert_eq!(waypoint.current, true);
        assert!(matches!(waypoint.frame, MavFrame::Mission));
        assert!(matches!(waypoint.command, MavCmdWaypoint::NavWaypoint));
        assert_eq!(waypoint.param1, 0.149999999999999994);
        assert_eq!(waypoint.param2, 2.0);
        assert_eq!(waypoint.param3, 3.0);
        assert_eq!(waypoint.param4, 4.0);
        assert_eq!(waypoint.x, 8.54800000000000004);
        assert_eq!(waypoint.y, 47.3759999999999977);
        assert_eq!(waypoint.z, 550.0);
        assert!(waypoint.autocontinue);
    }

    #[test]
    fn test_parse_waypoint_from_bytes() {
        let bytes = "12\t1\t2\t16\t0.149999999999999994\t2.0\t3.0\t4.0\t8.54800000000000004\t47.3759999999999977\t550\t1".as_bytes();
        let waypoint = Waypoint::try_from(bytes).unwrap();

        assert_eq!(waypoint.index, 12);
        assert_eq!(waypoint.current, true);
        assert!(matches!(waypoint.frame, MavFrame::Mission));
        assert!(matches!(waypoint.command, MavCmdWaypoint::NavWaypoint));
        assert_eq!(waypoint.param1, 0.149999999999999994);
        assert_eq!(waypoint.param2, 2.0);
        assert_eq!(waypoint.param3, 3.0);
        assert_eq!(waypoint.param4, 4.0);
        assert_eq!(waypoint.x, 8.54800000000000004);
        assert_eq!(waypoint.y, 47.3759999999999977);
        assert_eq!(waypoint.z, 550.0);
        assert!(waypoint.autocontinue);
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn test_waypoint_to_string() {
        use crate::alloc::string::ToString;

        let original = "12\t1\t2\t16\t0.15\t2\t3\t4\t8.548\t47.376\t550\t1";
        let waypoint = Waypoint::try_from(original).unwrap();
        let dumped = waypoint.to_string();

        assert_eq!(dumped, original);
    }

    #[test]
    fn test_waypoint_as_str_no_alloc() {
        let original = "12\t1\t2\t16\t0.15\t2\t3\t4\t8.548\t47.376\t550\t1";
        let waypoint = Waypoint::try_from(original).unwrap();
        let mut buf = [0u8; 255];
        let dumped = waypoint.as_str_buffered(buf.as_mut_slice()).unwrap();
        assert_eq!(dumped, original);
    }

    #[test]
    fn test_waypoint_as_bytes_no_alloc() {
        let original = "12\t1\t2\t16\t0.15\t2\t3\t4\t8.548\t47.376\t550\t1";
        let waypoint = Waypoint::try_from(original).unwrap();
        let mut buf = [0u8; 255];
        let dumped = waypoint.as_bytes_buffered(buf.as_mut_slice()).unwrap();
        assert_eq!(dumped, original.as_bytes());
    }

    #[test]
    fn test_waypoint_to_str_no_alloc_too_small_buffer() {
        let original = "12\t1\t2\t16\t0.15\t2\t3\t4\t8.548\t47.376\t550\t1";
        let waypoint = Waypoint::try_from(original).unwrap();
        let mut buf = [0u8; 10];
        assert!(matches!(
            waypoint.as_str_buffered(buf.as_mut_slice()),
            Err(MissionError::BufferTooSmall)
        ));
    }
}
