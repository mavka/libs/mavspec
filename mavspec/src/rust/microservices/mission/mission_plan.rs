use super::consts::{DEFAULT_MISSION_PLAN_VERSION, MISSION_PLAN_FILE_PREFIX};
use super::error::MissionError;
use super::Waypoint;

/// <sup>`⍚`</sup> Mission plan for drones that support MAVLink [mission](https://mavlink.io/en/services/mission.html)
/// protocol.
///
/// Supports unofficial [mission file format](https://mavlink.io/en/file_formats/#mission_plain_text_file).
///
/// For `no-alloc` targets use [`no_alloc::MissionPlan`](super::no_alloc::MissionPlan).
#[cfg_attr(all(feature = "specta", feature = "unstable"), derive(specta::Type))]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg(feature = "alloc")]
#[derive(Clone, Debug)]
pub struct MissionPlan {
    version: u16,
    waypoints: alloc::vec::Vec<Waypoint>,
}

impl MissionPlan {
    /// Creates a new empty mission plan.
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns a slice of waypoints.
    pub fn waypoints(&self) -> &[Waypoint] {
        self.waypoints.as_slice()
    }

    /// Returns the length of available waypoints.
    pub fn len(&self) -> usize {
        self.waypoints.len()
    }

    /// Returns `true` if the list of waypoints is empty.
    pub fn is_empty(&self) -> bool {
        self.waypoints.is_empty()
    }

    /// Puts waypoint into the mission plan.
    ///
    /// This function will either insert a waypoint to an already existing position or append list
    /// of waypoints.
    pub fn put(&mut self, waypoint: Waypoint) -> Option<u16> {
        if waypoint.index > self.len() as u16 {
            return None;
        }

        let idx = waypoint.index;
        if idx as usize >= self.waypoints.len() {
            self.waypoints.push(waypoint);
        } else {
            self.waypoints[waypoint.index as usize] = waypoint;
        }

        Some(idx)
    }

    /// Inserts waypoint into the mission plan.
    ///
    /// This function will insert a waypoint and adjust other waypoint positions accordingly. If the
    /// `index` of a waypoint is greater than length of the current mission plan, it will be
    /// ignored.
    pub fn insert(&mut self, waypoint: Waypoint) -> Option<u16> {
        if waypoint.index > self.len() as u16 {
            return None;
        }

        let idx = waypoint.index;
        self.waypoints.insert(idx as usize, waypoint);
        let len = self.waypoints.len();

        for (n, entry) in self.waypoints[idx as usize..len].iter_mut().enumerate() {
            entry.index = n as u16;
        }

        Some(idx)
    }

    /// Append a new waypoint.
    ///
    /// This will update the `index` field of a [`Waypoint`].
    pub fn push(&mut self, waypoint: Waypoint) -> Option<u16> {
        let mut waypoint = waypoint;
        let idx = self.len() as u16;
        waypoint.index = idx;
        let waypoint = waypoint;

        self.waypoints.push(waypoint);

        Some(idx)
    }

    /// Takes the last [`Waypoint`] from the mission list.
    pub fn pop(&mut self) -> Option<Waypoint> {
        self.waypoints.pop()
    }
}

impl Default for MissionPlan {
    fn default() -> Self {
        Self {
            version: DEFAULT_MISSION_PLAN_VERSION,
            waypoints: Default::default(),
        }
    }
}

impl From<&[Waypoint]> for MissionPlan {
    fn from(value: &[Waypoint]) -> Self {
        Self {
            version: DEFAULT_MISSION_PLAN_VERSION,
            waypoints: value.to_vec(),
        }
    }
}

impl TryFrom<&[u8]> for MissionPlan {
    type Error = MissionError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        use core::str;
        Self::try_from(str::from_utf8(value).map_err(|_| Self::Error::InvalidUtf8)?)
    }
}

impl TryFrom<&str> for MissionPlan {
    type Error = MissionError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut waypoints = MissionPlan::new();

        for (idx, line) in value
            .split('\n')
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .enumerate()
        {
            if line.starts_with(MISSION_PLAN_FILE_PREFIX) {
                waypoints.version = line
                    .trim_start_matches(MISSION_PLAN_FILE_PREFIX)
                    .parse::<u16>()
                    .unwrap_or(DEFAULT_MISSION_PLAN_VERSION);
                continue;
            }

            match Waypoint::try_from(line) {
                Ok(waypoint) => {
                    if waypoints.push(waypoint).is_none() {
                        return Err(Self::Error::OutOfOrder(waypoint.index));
                    }
                }
                Err(_) if idx == 0 => continue,
                Err(err) => return Err(err),
            }
        }

        Ok(waypoints)
    }
}

impl core::fmt::Display for MissionPlan {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!(
            "{}{}\n",
            MISSION_PLAN_FILE_PREFIX, self.version
        ))?;
        for waypoint in self.waypoints.iter() {
            f.write_fmt(format_args!("{}\n", waypoint))?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod mission_plan_tests {
    use super::*;

    #[test]
    fn test_parse_mission_plan_from_str() {
        let contents = "\
        0\t1\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n\
        1\t0\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n";

        MissionPlan::try_from(contents).unwrap();
    }

    #[test]
    fn test_parse_mission_plan_from_bytes() {
        let contents = "\
        0\t1\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n\
        1\t0\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n";

        MissionPlan::try_from(contents.as_bytes()).unwrap();
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn test_mission_plan_to_string() {
        use crate::alloc::string::ToString;

        let original = "QGC WPL 110\n\
        0\t1\t0\t16\t0\t0\t0\t0\t50.4338103\t30.5059493\t12.19\t1\n\
        1\t0\t3\t22\t15\t0\t0\t0\t50.4338203\t50.4338203\t50\t1\n\
        2\t0\t3\t16\t0\t0\t0\t0\t50.4338303\t50.4338303\t50\t1\n";

        let mission = MissionPlan::try_from(original).unwrap();
        assert_eq!(mission.version, 110);

        let dumped = mission.to_string();
        assert_eq!(original, dumped);
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn test_new_mission_plan_empty() {
        let mission = MissionPlan::new();
        assert!(mission.is_empty());
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn test_mission_paln_push_pop() {
        let mut mission = MissionPlan::new();
        assert!(mission.is_empty());

        assert!(matches!(mission.push(Waypoint::default()), Some(0)));
        assert!(matches!(mission.push(Waypoint::default()), Some(1)));

        let waypoint = mission.pop().unwrap();
        assert_eq!(waypoint.index, 1);
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn test_mission_plan_put() {
        let mut mission = MissionPlan::new();
        assert!(mission.is_empty());

        assert!(matches!(mission.push(Waypoint::default()), Some(0)));
        assert!(matches!(mission.push(Waypoint::default()), Some(1)));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 0,
                ..Default::default()
            }),
            Some(0)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 1,
                ..Default::default()
            }),
            Some(1)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 2,
                ..Default::default()
            }),
            Some(2)
        ));
        assert_eq!(mission.len(), 3);

        assert!(matches!(
            mission.put(Waypoint {
                index: 4,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 3);
    }

    #[test]
    #[cfg(feature = "alloc")]
    fn test_mission_plan_insert() {
        let mut mission = MissionPlan::new();

        assert!(matches!(
            mission.insert(Waypoint {
                index: 0,
                param1: 2.0,
                ..Default::default()
            }),
            Some(0)
        ));
        assert_eq!(mission.len(), 1);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 0,
                param1: 1.0,
                ..Default::default()
            }),
            Some(0)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 2,
                param1: 3.0,
                ..Default::default()
            }),
            Some(2)
        ));
        assert_eq!(mission.len(), 3);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 4,
                param1: 5.0,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 3);

        assert_eq!(mission.waypoints()[0].param1, 1.0);
        assert_eq!(mission.waypoints()[1].param1, 2.0);
        assert_eq!(mission.waypoints()[2].param1, 3.0);
    }
}
