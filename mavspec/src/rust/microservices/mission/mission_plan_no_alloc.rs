use super::consts::{DEFAULT_MISSION_PLAN_VERSION, MISSION_PLAN_FILE_PREFIX};
use super::error::MissionError;
use super::Waypoint;

/// <sup>`⍚`</sup> Mission plan for drones that support MAVLink [mission](https://mavlink.io/en/services/mission.html)
/// protocol (`no-alloc` version).
#[derive(Clone, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct MissionPlan<const N: usize> {
    version: u16,
    #[cfg_attr(feature = "serde", serde(with = "serde_arrays"))]
    waypoints: [Waypoint; N],
    len: usize,
}

impl<const N: usize> MissionPlan<N> {
    /// Creates a new empty mission plan.
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns a slice of waypoints.
    pub fn waypoints(&self) -> &[Waypoint] {
        &self.waypoints[0..self.len]
    }

    /// Returns the length of available waypoints.
    pub fn len(&self) -> usize {
        self.len
    }

    /// Returns `true` if the list of waypoints is empty.
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// Puts waypoint into the mission plan.
    ///
    /// This function will either insert a waypoint to an already existing position or append list
    /// of waypoints.
    pub fn put(&mut self, waypoint: Waypoint) -> Option<u16> {
        if waypoint.index >= N as u16 || waypoint.index > self.len as u16 {
            return None;
        }

        let idx = waypoint.index;
        self.waypoints[idx as usize] = waypoint;
        self.len = core::cmp::max((idx + 1) as usize, self.len);

        Some(idx)
    }

    /// Inserts waypoint into the mission plan.
    ///
    /// This function will insert a waypoint and adjust other waypoint positions accordingly. If the
    /// `index` of a waypoint is greater than length of the current mission plan, it will be
    /// ignored.
    pub fn insert(&mut self, waypoint: Waypoint) -> Option<u16> {
        if self.len >= N || waypoint.index >= N as u16 || waypoint.index > self.len as u16 {
            return None;
        }

        let idx = waypoint.index;
        self.len += 1;
        let len = self.waypoints.len();
        let mut next = waypoint;

        for (n, entry) in self.waypoints[idx as usize..len].iter_mut().enumerate() {
            next.index = n as u16;
            core::mem::swap(entry, &mut next);
        }

        Some(idx)
    }

    /// Append a new waypoint.
    ///
    /// This will update the `index` field of a [`Waypoint`].
    pub fn push(&mut self, waypoint: Waypoint) -> Option<u16> {
        if self.len >= N {
            return None;
        }

        let mut waypoint = waypoint;
        let idx = self.len as u16;
        waypoint.index = idx;
        let waypoint = waypoint;

        self.waypoints[idx as usize] = waypoint;
        self.len += 1;

        Some(idx)
    }

    /// Takes the last [`Waypoint`] from the mission list.
    pub fn pop(&mut self) -> Option<Waypoint> {
        if self.len > 0 {
            let mut waypoint = Waypoint::default();
            core::mem::swap(&mut self.waypoints[self.len - 1], &mut waypoint);
            self.len -= 1;
            return Some(waypoint);
        }

        None
    }

    /// Tries to create an `&str` representations of mission plan using a provided buffer.
    ///
    /// Supports `no-alloc` targets.
    pub fn as_str_buffered<'a>(&self, buf: &'a mut [u8]) -> Result<&'a str, MissionError> {
        let mut len = 0;

        len += format_no_std::show(
            &mut buf[..],
            format_args!("{}{}\n", MISSION_PLAN_FILE_PREFIX, self.version),
        )
        .map_err(|_| MissionError::BufferTooSmall)?
        .as_bytes()
        .len();

        for waypoint in self.waypoints.iter() {
            len += format_no_std::show(&mut buf[len..], format_args!("{}\n", waypoint))
                .map_err(|_| MissionError::BufferTooSmall)?
                .as_bytes()
                .len();
        }

        use core::str;
        str::from_utf8(&buf[..len]).map_err(|_| MissionError::InvalidUtf8)
    }

    /// Tries to create a byte representations of mission plan using a provided buffer.
    ///
    /// Supports `no-alloc` targets.
    pub fn as_bytes_buffered<'a>(&self, buf: &'a mut [u8]) -> Result<&'a [u8], MissionError> {
        Ok(self.as_str_buffered(buf)?.as_bytes())
    }
}

impl<const N: usize> Default for MissionPlan<N> {
    fn default() -> Self {
        Self {
            version: DEFAULT_MISSION_PLAN_VERSION,
            waypoints: [Waypoint::default(); N],
            len: 0,
        }
    }
}

impl<const N: usize> TryFrom<&[Waypoint]> for MissionPlan<N> {
    type Error = MissionError;

    fn try_from(value: &[Waypoint]) -> Result<Self, Self::Error> {
        if value.len() > N {
            return Err(MissionError::BufferTooSmall);
        }

        let len = value.len();
        let mut waypoints = [Waypoint::default(); N];
        for (idx, waypoint) in value.iter().enumerate() {
            waypoints[idx] = waypoint.clone();
        }

        Ok(Self {
            version: DEFAULT_MISSION_PLAN_VERSION,
            waypoints,
            len,
        })
    }
}

impl<const N: usize> TryFrom<&str> for MissionPlan<N> {
    type Error = MissionError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut waypoints = MissionPlan::<N>::new();

        for (idx, line) in value
            .split('\n')
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .enumerate()
        {
            if line.starts_with(MISSION_PLAN_FILE_PREFIX) {
                waypoints.version = line
                    .trim_start_matches(MISSION_PLAN_FILE_PREFIX)
                    .parse::<u16>()
                    .unwrap_or(DEFAULT_MISSION_PLAN_VERSION);
                continue;
            }

            match Waypoint::try_from(line) {
                Ok(waypoint) => {
                    if waypoints.len() >= N {
                        return Err(Self::Error::BufferTooSmall);
                    }
                    if waypoints.push(waypoint).is_none() {
                        return Err(Self::Error::OutOfOrder(waypoint.index));
                    }
                }
                Err(_) if idx == 0 => continue,
                Err(err) => return Err(err),
            }
        }

        Ok(waypoints)
    }
}

impl<const N: usize> TryFrom<&[u8]> for MissionPlan<N> {
    type Error = MissionError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        use core::str;
        Self::try_from(str::from_utf8(value).map_err(|_| Self::Error::InvalidUtf8)?)
    }
}

#[cfg(test)]
mod no_alloc_mission_plan_tests {
    use super::*;

    #[test]
    fn test_parse_mission_plan_from_str() {
        let contents = "\
        0\t1\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n\
        1\t0\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n";

        MissionPlan::<2>::try_from(contents).unwrap();
    }

    #[test]
    fn test_parse_mission_plan_from_bytes() {
        let contents = "\
        0\t1\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n\
        1\t0\t0\t16\t0.149999999999999994\t0\t0\t0\t8.54800000000000004\t47.3759999999999977\t550\t1\n";

        MissionPlan::<2>::try_from(contents.as_bytes()).unwrap();
    }

    #[test]
    fn test_mission_plan_as_str_buffered() {
        let original = "QGC WPL 110\n\
        0\t1\t0\t16\t0\t0\t0\t0\t50.4338103\t30.5059493\t12.19\t1\n\
        1\t0\t3\t22\t15\t0\t0\t0\t50.4338203\t50.4338203\t50\t1\n\
        2\t0\t3\t16\t0\t0\t0\t0\t50.4338303\t50.4338303\t50\t1\n";

        let mission = MissionPlan::<3>::try_from(original).unwrap();
        assert_eq!(mission.version, 110);

        let mut buf = [0u8; 1024];
        let dumped = mission.as_str_buffered(&mut buf).unwrap();
        assert_eq!(original, dumped);
    }

    #[test]
    fn test_mission_plan_as_bytes_buffered() {
        let original = "QGC WPL 110\n\
        0\t1\t0\t16\t0\t0\t0\t0\t50.4338103\t30.5059493\t12.19\t1\n\
        1\t0\t3\t22\t15\t0\t0\t0\t50.4338203\t50.4338203\t50\t1\n\
        2\t0\t3\t16\t0\t0\t0\t0\t50.4338303\t50.4338303\t50\t1\n";

        let mission = MissionPlan::<3>::try_from(original).unwrap();
        assert_eq!(mission.version, 110);

        let mut buf = [0u8; 1024];
        let dumped = mission.as_bytes_buffered(&mut buf).unwrap();
        assert_eq!(dumped, original.as_bytes());
    }

    #[test]
    fn test_no_alloc_mission_plan_empty() {
        let mut mission = MissionPlan::<0>::new();
        assert!(mission.is_empty());

        assert!(matches!(mission.push(Waypoint::default()), None));
        assert!(matches!(mission.put(Waypoint::default()), None));
        assert!(matches!(mission.insert(Waypoint::default()), None));
    }

    #[test]
    fn test_no_alloc_mission_plan_bounds() {
        let mut mission = MissionPlan::<2>::new();
        assert!(mission.is_empty());

        assert!(matches!(mission.push(Waypoint::default()), Some(0)));
        assert!(matches!(mission.push(Waypoint::default()), Some(1)));
        assert_eq!(mission.len(), 2);

        assert!(matches!(mission.pop(), Some(_)));
        assert_eq!(mission.len(), 1);
        assert!(matches!(mission.push(Waypoint::default()), Some(1)));
        assert_eq!(mission.len(), 2);
        assert!(matches!(mission.pop(), Some(_)));
        assert_eq!(mission.len(), 1);
        assert!(matches!(
            mission.put(Waypoint {
                index: 1,
                ..Default::default()
            }),
            Some(1)
        ));
        assert_eq!(mission.len(), 2);
        assert!(matches!(mission.pop(), Some(_)));
        assert_eq!(mission.len(), 1);
        assert!(matches!(
            mission.insert(Waypoint {
                index: 1,
                ..Default::default()
            }),
            Some(1)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(mission.push(Waypoint::default()), None));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 2,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 1,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 2);
        assert!(matches!(
            mission.insert(Waypoint {
                index: 2,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 2);
    }

    #[test]
    fn test_no_alloc_new_mission_plan_push_pop() {
        let mut mission = MissionPlan::<2>::new();
        assert!(mission.is_empty());

        assert!(matches!(mission.push(Waypoint::default()), Some(0)));
        assert!(matches!(mission.push(Waypoint::default()), Some(1)));
        assert!(matches!(mission.push(Waypoint::default()), None));

        let waypoint = mission.pop().unwrap();
        assert_eq!(waypoint.index, 1);
        let waypoint = mission.pop().unwrap();
        assert_eq!(waypoint.index, 0);

        assert!(matches!(mission.pop(), None));
    }

    #[test]
    fn test_no_alloc_mission_plan_put() {
        let mut mission = MissionPlan::<10>::new();
        assert!(mission.is_empty());

        assert!(matches!(mission.push(Waypoint::default()), Some(0)));
        assert!(matches!(mission.push(Waypoint::default()), Some(1)));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 0,
                ..Default::default()
            }),
            Some(0)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 1,
                ..Default::default()
            }),
            Some(1)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.put(Waypoint {
                index: 2,
                ..Default::default()
            }),
            Some(2)
        ));
        assert_eq!(mission.len(), 3);

        assert!(matches!(
            mission.put(Waypoint {
                index: 4,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 3);
    }

    #[test]
    fn test_no_alloc_mission_plan_insert() {
        let mut mission = MissionPlan::<10>::new();

        assert!(matches!(
            mission.insert(Waypoint {
                index: 0,
                param1: 2.0,
                ..Default::default()
            }),
            Some(0)
        ));
        assert_eq!(mission.len(), 1);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 0,
                param1: 1.0,
                ..Default::default()
            }),
            Some(0)
        ));
        assert_eq!(mission.len(), 2);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 2,
                param1: 3.0,
                ..Default::default()
            }),
            Some(2)
        ));
        assert_eq!(mission.len(), 3);

        assert!(matches!(
            mission.insert(Waypoint {
                index: 4,
                param1: 5.0,
                ..Default::default()
            }),
            None
        ));
        assert_eq!(mission.len(), 3);

        assert_eq!(mission.waypoints()[0].param1, 1.0);
        assert_eq!(mission.waypoints()[1].param1, 2.0);
        assert_eq!(mission.waypoints()[2].param1, 3.0);
    }
}
