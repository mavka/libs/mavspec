/// <sup>`⍚`</sup> Mission-related errors.
#[derive(Copy, Clone, Debug)]
#[cfg_attr(all(feature = "specta", feature = "unstable"), derive(specta::Type))]
#[cfg_attr(
    all(feature = "specta", feature = "unstable"),
    specta(rename = "MavioMissionError")
)]
#[cfg_attr(
    all(feature = "serde", feature = "unstable"),
    derive(serde::Serialize, serde::Deserialize)
)]
#[cfg_attr(feature = "std", derive(thiserror::Error))]
pub enum MissionError {
    /// Field parse error.
    #[cfg_attr(feature = "std", error("unable to parse field at index {0:?}"))]
    ParseField(usize),
    /// Missing field.
    #[cfg_attr(feature = "std", error("unable to read field at index {0:?}"))]
    MissingField(usize),
    /// Invalid UTF8 representation.
    #[cfg_attr(feature = "std", error("invalid UTF8 representation"))]
    InvalidUtf8,
    /// Waypoint is out of order.
    #[cfg_attr(feature = "std", error("waypoint {0:?} is out of order"))]
    OutOfOrder(u16),
    /// Buffer is too small.
    #[cfg_attr(feature = "std", error("buffer is too small"))]
    BufferTooSmall,
}
