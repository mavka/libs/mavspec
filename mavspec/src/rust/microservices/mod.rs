//! <sup>`⍚`</sup> Tools for MAVLink [microservices](https://mavlink.io/en/services/).

#[cfg(feature = "msrv-utils-mission")]
pub mod mission;
