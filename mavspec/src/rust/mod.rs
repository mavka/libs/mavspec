//! # MAVSpec's code generation toolchain for Rust
#![cfg_attr(
    feature = "rust_gen",
    doc = "\n\nCheck [`gen`] module documentation to learn how to build additional MAVLink dialects."
)]

#[doc(inline)]
pub use mavspec_rust_derive as derive;
#[doc(inline)]
pub use mavspec_rust_spec as spec;

#[cfg(feature = "rust_gen")]
#[doc(inline)]
pub use mavspec_rust_gen as gen;

#[doc(inline)]
#[cfg(feature = "rust-dialects")]
pub use mavlink_dialects::dialects;

#[cfg(feature = "dlct-minimal")]
#[cfg(feature = "rust-dialects")]
#[doc(inline)]
pub use mavlink_dialects::{default_dialect, DefaultDialect};

/// <sup>`⍚`</sup>
#[cfg(all(feature = "msrv-utils", feature = "unstable"))]
pub mod microservices;
