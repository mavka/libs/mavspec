# MAVLink dialects bindings for [MAVSpec](https://crates.io/crates/mavspec)

<span style="font-size:24px">[🇺🇦](https://mavka.gitlab.io/home/a_note_on_the_war_in_ukraine/)</span>
[![
`repository`](https://img.shields.io/gitlab/pipeline-status/mavka/libs/mavspec.svg?branch=main&label=repository)](https://gitlab.com/mavka/libs/mavspec)
[![`crates.io`](https://img.shields.io/crates/v/mavlink-dialects.svg)](https://crates.io/crates/mavlink-dialects)
[![
`docs.rs`](https://img.shields.io/docsrs/mavlink-dialects.svg?label=docs.rs)](https://docs.rs/mavlink-dialects/latest/mavlink-dialects/)
[![
`issues`](https://img.shields.io/gitlab/issues/open/mavka/libs/mavspec.svg)](https://gitlab.com/mavka/libs/mavspec/-/issues/)

> ⚠️ All issues related to this crate should be submitted to [MAVSpec](https://gitlab.com/mavka/libs/mavspec).

[MAVLink](https://mavlink.io/en/) messages bindings based on [MAVInspect](https://crates.io/crates/mavinspect).

This library is a part of [`MAVSpec`](https://crates.io/crates/mavspec) code-generation
toolchain and is intended to use alongside with it. However, nothing prevents you from using
this crate independently.

Since [`MAVSpec`](https://crates.io/crates/mavspec) is I/O agnostic, it and does not provide any
abstractions for transferring MAVLink messages. If you want to communicate with MAVLink devices,
use [Mavio](https://crates.io/crates/mavio) for embedded devices and simple tasks or
[Maviola](https://crates.io/crates/maviola) for advanced I/O in `std` environments (for ground
control stations, communication layers, and so on).

Upstream crates that use [Mavka](https://mavka.gitlab.io/home/) libraries toolchain can use
Cargo [patch](https://doc.rust-lang.org/cargo/reference/overriding-dependencies.html#the-patch-section)
mechanism to replace this crate and therefore change which dialects will be packaged.

Usage
-----

This crate exposes `dialects` module where all MAVlink dialects can be found.

```rust
fn main() {
    let protocol = mavlink_dialects::common::Common;
    let common = protocol.get_dialect_by_name("common").unwrap();
    let heartbeat_message = common.get_message_by_name("HEARTBEAT").unwrap();
    assert_eq!(heartbeat_message.defined_in().unwrap(), "minimal");
}
```

Development
-----------

The main development workflow for this crate involves inclusion into
[MAVSpec](https://gitlab.com/mavka/libs/mavspec) as a submodule.

License
-------

> Here we simply comply with the suggested dual licensing according to
> [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html) (C-PERMISSIVE).

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](../LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](../LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

Contribution
------------

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
