#[cfg(feature = "dlct-minimal")]
fn play_with_dialects() -> Result<(), mavlink_dialects::spec::SpecError> {
    use default_dialect::messages::Heartbeat;
    use mavlink_dialects::default_dialect::enums::{MavAutopilot, MavModeFlag, MavState};
    use mavlink_dialects::spec::*;
    use mavlink_dialects::{default_dialect, DefaultDialect};

    let message = Heartbeat {
        autopilot: MavAutopilot::Armazila,
        base_mode: MavModeFlag::HIL_ENABLED,
        system_status: MavState::Active,
        ..Default::default()
    };

    // Check that this message indeed supports `MAVLink 1`.
    assert_eq!(message.min_supported_mavlink_version(), MavLinkVersion::V1);

    // Encode to MAVLink payload.
    let payload = message.encode(MavLinkVersion::V2)?;
    assert_eq!(payload.version(), MavLinkVersion::V2);
    assert_eq!(payload.id(), message.id());

    // Decode from MAVLink payload, now without .
    let dialect_message = DefaultDialect::decode(&payload)?;
    match dialect_message {
        DefaultDialect::Heartbeat(msg) => {
            assert_eq!(msg.id(), Heartbeat::ID);
        }
        _ => panic!("Unexpected dialect message: {:?}", dialect_message),
    }

    Ok(())
}

fn main() {
    #[cfg(feature = "dlct-minimal")]
    play_with_dialects().expect("Failed to play with mavlink dialects");
}

#[cfg(test)]
#[cfg(feature = "dlct-minimal")]
#[test]
fn test_mavlink_dialects_default_dialect_example() {
    play_with_dialects().expect("Failed to play with mavlink dialects");
}
