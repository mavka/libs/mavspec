use quote::quote;

pub(crate) fn make_serde_derive_annotation(enabled: bool) -> proc_macro2::TokenStream {
    if enabled {
        quote! {
            #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
        }
    } else {
        quote!()
    }
}

pub(crate) fn make_specta_derive_annotation(
    enabled: bool,
    rename: Option<&str>,
) -> proc_macro2::TokenStream {
    if enabled {
        let annotations = quote! {
            #[cfg_attr(feature = "specta", derive(specta::Type))]
        };
        let annotations = match rename {
            Some(new_name) => {
                quote! {
                    #annotations
                    #[cfg_attr(feature = "specta", specta(rename = #new_name))]
                }
            }
            None => annotations,
        };
        annotations
    } else {
        quote!()
    }
}
