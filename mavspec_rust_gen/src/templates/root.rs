use crate::specs::root::RootModuleSpec;

use crate::conventions::dialect_enum_name;
use crate::utils::dialect_module_name;
use quote::{format_ident, quote};

/// Root module template.
pub fn root_module(spec: &RootModuleSpec) -> syn::File {
    let default_dialects = match spec.default_dialect() {
        Some(canonical_name) => {
            let dialect_module_name = format_ident!("{}", dialect_module_name(canonical_name));
            let dialect_enum_ident = format_ident!("{}", dialect_enum_name(canonical_name));

            quote! {
                /// Default standard MAVLink dialect module.
                pub use dialects::#dialect_module_name as default_dialect;

                /// Default standard MAVLink dialect.
                pub use dialects::#dialect_module_name::#dialect_enum_ident as DefaultDialect;
            }
        }
        None => quote! {},
    };

    syn::parse2(quote! {
        // MAVLink protocol definition.
        //
        // Since this file is intended to be included with `include!`, we can not  provide module
        // documentation and leave this responsibility to the client.
        //
        // In most cases it makes sense to re-export `dialects` module:
        //
        // ```
        // mod mavlink {
        //     include!(concat!(env!("OUT_DIR"), "/mavlink/mod.rs"));
        // }
        // pub use mavlink::dialects;
        // ```
        //
        // Although this is possible, we do not suggest to use `!include` without `mod` wrapper since
        // newer versions may introduce extra definitions that may interfere with your source code.

        // Import all dialects
        pub mod dialects;

        #default_dialects
    })
    .unwrap()
}
