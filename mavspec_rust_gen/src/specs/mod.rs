pub(crate) mod dialects;
pub(crate) mod root;

use crate::generator::GeneratorParams;

pub(crate) trait Spec {
    fn params(&self) -> &GeneratorParams;
}
