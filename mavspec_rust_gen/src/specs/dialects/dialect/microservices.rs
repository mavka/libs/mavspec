use serde::Serialize;

use mavinspect::protocol::Microservices;

use crate::generator::GeneratorParams;
use crate::specs::dialects::dialect::DialectModuleSpec;
use crate::specs::Spec;

/// Specification for microservices root module template.
#[derive(Clone, Debug, Serialize)]
pub(crate) struct MicroservicesRootModuleSpec<'a> {
    microservices: Microservices,
    params: &'a GeneratorParams,
}

impl<'a> Spec for MicroservicesRootModuleSpec<'a> {
    fn params(&self) -> &'a GeneratorParams {
        self.params
    }
}

impl<'a> MicroservicesRootModuleSpec<'a> {
    pub(crate) fn new(dialect_spec: &'a DialectModuleSpec<'_>) -> Self {
        Self {
            microservices: dialect_spec.microservices() & dialect_spec.params().microservices,
            params: dialect_spec.params(),
        }
    }

    pub(crate) fn microservices(&self) -> &Microservices {
        &self.microservices
    }
}
