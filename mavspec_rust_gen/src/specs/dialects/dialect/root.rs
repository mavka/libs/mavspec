use mavinspect::protocol::Microservices;
use mavinspect::protocol::{Dialect, DialectId, DialectVersion, Enum, Message};
use serde::Serialize;

use crate::conventions::{dialect_enum_name, dialect_mod_name};
use crate::generator::GeneratorParams;
use crate::specs::Spec;

/// Specification for microservice.
#[derive(Clone, Debug, Serialize)]
pub(crate) struct MsrvSpec<'a> {
    pub(super) name: &'a str,
    pub(super) parent: &'a Dialect,
}

/// Specification for dialect module template.
#[derive(Clone, Debug, Serialize)]
pub(crate) struct DialectModuleSpec<'a> {
    dialect: &'a Dialect,
    messages: Vec<&'a Message>,
    enums: Vec<&'a Enum>,
    params: &'a GeneratorParams,
    microservices: Microservices,
    has_microservices: bool,
    msrv: Option<MsrvSpec<'a>>,
}

impl<'a> MsrvSpec<'a> {
    pub(crate) fn new(name: &'a str, parent: &'a Dialect) -> Self {
        Self { name, parent }
    }

    pub(crate) fn name(&self) -> &'a str {
        self.name
    }

    pub(crate) fn parent(&self) -> &'a Dialect {
        self.parent
    }
}

impl<'a> Spec for DialectModuleSpec<'a> {
    fn params(&self) -> &'a GeneratorParams {
        self.params
    }
}

impl<'a> DialectModuleSpec<'a> {
    pub(crate) fn new(
        dialect: &'a Dialect,
        params: &'a GeneratorParams,
        has_microservices: bool,
    ) -> Self {
        Self {
            dialect,
            messages: Vec::from_iter(dialect.messages()),
            enums: Vec::from_iter(dialect.enums()),
            params,
            microservices: dialect.microservices() & params.microservices,
            has_microservices,
            msrv: None,
        }
    }

    pub(crate) fn with_msrv(self, msrv: MsrvSpec<'a>) -> Self {
        Self {
            msrv: Some(msrv),
            ..self
        }
    }

    pub(crate) fn canonical_name(&self) -> &str {
        self.dialect.canonical_name()
    }

    pub(crate) fn module_name(&self) -> String {
        dialect_mod_name(self.dialect.canonical_name())
    }

    pub(crate) fn enum_name(&self) -> String {
        dialect_enum_name(self.dialect.canonical_name())
    }

    pub(crate) fn display_name(&self) -> &str {
        self.dialect.name()
    }

    pub(crate) fn messages(&self) -> &[&Message] {
        self.messages.as_slice()
    }

    pub(crate) fn enums(&self) -> &[&Enum] {
        self.enums.as_slice()
    }

    pub(crate) fn dialect_id(&self) -> Option<DialectId> {
        self.dialect.dialect()
    }

    pub(crate) fn version(&self) -> Option<DialectVersion> {
        self.dialect.version()
    }

    pub(crate) fn get_enum_by_name(&self, name: &str) -> Option<&Enum> {
        self.enums
            .iter()
            .find(|&&mav_enum| mav_enum.name() == name)
            .copied()
    }

    pub(crate) fn dialect(&self) -> &'a Dialect {
        self.dialect
    }

    pub(crate) fn microservices(&self) -> Microservices {
        self.microservices
    }

    pub(crate) fn has_microservices(&self) -> bool {
        self.has_microservices
    }

    pub(crate) fn msrv(&self) -> Option<&MsrvSpec<'a>> {
        self.msrv.as_ref()
    }

    pub(crate) fn msrv_name(&self) -> Option<&'a str> {
        self.msrv.as_ref().map(|msrv| msrv.name)
    }

    pub(crate) fn parent_dialect(&self) -> Option<&'a Dialect> {
        self.msrv.as_ref().map(|msrv| msrv.parent)
    }
}
