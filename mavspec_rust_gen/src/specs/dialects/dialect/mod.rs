mod root;
pub(crate) use root::DialectModuleSpec;
pub(crate) use root::MsrvSpec;

pub(crate) mod enums;
pub(crate) mod messages;
pub(crate) mod microservices;
