use mavinspect::protocol::Protocol;

/// Specification for root module.
pub(crate) struct RootModuleSpec<'a> {
    default_dialect: Option<&'a str>,
}

impl<'a> RootModuleSpec<'a> {
    pub(crate) fn new(protocol: &'a Protocol) -> Self {
        Self {
            default_dialect: protocol.default_dialect().map(|dlct| dlct.canonical_name()),
        }
    }

    pub(crate) fn default_dialect(&self) -> Option<&str> {
        self.default_dialect
    }
}
