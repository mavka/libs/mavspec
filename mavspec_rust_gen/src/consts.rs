pub(crate) const WAYPOINT_COMMANDS: &[&str] =
    &["MAV_CMD_NAV_*", "MAV_CMD_DO_*", "MAV_CMD_CONDITION_*"];
pub(crate) const MAV_CMD_WAYPOINT: &str = "MAV_CMD_WAYPOINT";
pub(crate) const MAV_CMD: &str = "MAV_CMD";
